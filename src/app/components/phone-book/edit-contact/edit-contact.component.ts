import { Component } from '@angular/core';
import { FormControl, FormGroup, Validators } from "@angular/forms";
import { ActivatedRoute, Params, Router } from "@angular/router";
import { ApiContactsService } from "../../../services/api-contacts.service";
import { Contact } from "../../../services/interfaces/contact";

@Component({
  selector: 'app-edit-contact',
  templateUrl: './edit-contact.component.html',
  styleUrls: ['./edit-contact.component.scss']
})
export class EditContactComponent {

  /**
   * Форма редактирования
   */
  public form: FormGroup

  /**
   * Сущность редактированного контакта
   * @private
   */
  private contact: Contact

  /**
   * ID контакта
   * @private
   */
  private contactId: number

  /**
   * Флаг загрузки контакта
   */
  public loading: boolean = true

  constructor(
    private route: ActivatedRoute,
    private router: Router,
    public apiContactService: ApiContactsService
  ) {
    this.route.params.subscribe((params: Params) => {
      // Записываем ID из параметра
      this.contactId = +params.id

      this.apiContactService.getById(this.contactId).subscribe(contact => {
        this.loading = true

        this.contact = contact
        /**
         * Получаем контакт по ID и заполняем форму значениями из полей записи
         */
        this.form = this.setFormDefaultValue(this.contact)
        this.loading = false
      })
    })
  }

  /**
   * Устанавливает значения полученного контакта в форму.
   * @param contact
   * @private
   */
  private setFormDefaultValue(contact: Contact) {
    return new FormGroup({
      fullName: new FormControl(this.contact.fullName, [
        Validators.required
      ]),
      phoneNumber: new FormControl(this.contact.phoneNumber, [
        Validators.required
      ]),
      comment: new FormControl(this.contact.comment, [
        Validators.required
      ]),
      favorite: new FormControl(this.contact.favorite, [
        Validators.required
      ])
    })
  }

  /**
   * Изменяем данные контакта
   */
  submit() {
    if (this.form.valid) {
      const date = new Date()

      const contact = {
        ...this.form.value,
        created: `${date.getDate()}-${date.getMonth() + 1}-${date.getFullYear()}`
      };

      this.apiContactService.change(this.contactId, contact).subscribe(() => {
        this.router.navigate(['/'])
      })

      // Сбрасываем форму
      this.form.reset()
    }
  }
}
