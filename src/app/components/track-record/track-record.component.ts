import { Component, OnInit } from '@angular/core';
import { Title } from "@angular/platform-browser";
import { FormArray, FormControl, FormGroup, Validators } from "@angular/forms";

@Component({
  selector: 'app-track-record',
  templateUrl: './track-record.component.html',
  styleUrls: ['./track-record.component.scss']
})
export class TrackRecordComponent implements OnInit {

  /**
   * Счетчик форм.
   */
  private counter: number = 0

  /**
   * Форма.
   */
  public form: FormGroup;

  constructor(private titleService: Title) {
    this.titleService.setTitle('Track Record')
    this.form = new FormGroup({
      allForms: new FormArray([])
    })
  }

  /**
   * Возвращает новую форму.
   */
  private static createForm() {
    return new FormGroup({
      propertyAddress: new FormControl('', [
        Validators.required
      ]),
      purchase: new FormGroup({
        purchaseDate: new FormControl('', [
          Validators.required
        ]),
        purchasePrice: new FormControl('', [
          Validators.required
        ]),
        rehab: new FormControl('', [
          Validators.required
        ])
      }),
      sale: new FormGroup({
        saleDate: new FormControl('', [
          Validators.required
        ]),
        salePrice: new FormControl('', [
          Validators.required
        ])
      })
    })
  }

  ngOnInit(): void {
    this.addDeal()
  }

  /**
   * Возвращает массив форм.
   */
  get getAllForms(): FormArray {
    return <FormArray>this.form.get('allForms')
  }

  /**
   * Добавляет новую форму в массив allForms
   */
  addDeal() {
    let form = TrackRecordComponent.createForm()

    // Добавляем новое свойство в объект формы, который указывает номер добавленной формы.
    form['counter'] = ++this.counter;

    (this.form.get('allForms') as FormArray).push(form)
  }

  /**
   * Выводит данные формы в консоль.
   */
  next() {
    if (this.form.valid) {
      console.log({...this.form.value.allForms})
    }
  }

  /**
   * Удаляет форму с указанным ID
   * @param i
   */
  removeForm(i: number) {
    (this.form.get('allForms') as FormArray).removeAt(i)
    this.counter--;
  }
}
