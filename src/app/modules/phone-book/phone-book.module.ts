import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { PhoneBookComponent } from "../../components/phone-book/phone-book.component";
import { EditContactComponent } from "../../components/phone-book/edit-contact/edit-contact.component";
import { RouterModule } from "@angular/router";
import { ReactiveFormsModule } from "@angular/forms";

@NgModule({
  declarations: [
    PhoneBookComponent,
    EditContactComponent,
  ],
  imports: [
    CommonModule,
    ReactiveFormsModule,
    RouterModule.forChild([
      {
        path: '',
        component: PhoneBookComponent
      },
      {
        path: 'edit/:id',
        component: EditContactComponent
      }
    ])
  ],
  exports: [
    RouterModule
  ]
})
export class PhoneBookModule { }
