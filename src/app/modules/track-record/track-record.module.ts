import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { TrackRecordComponent } from "../../components/track-record/track-record.component";
import { RouterModule } from "@angular/router";
import { ReactiveFormsModule } from "@angular/forms";

@NgModule({
  declarations: [
    TrackRecordComponent
  ],
  imports: [
    CommonModule,
    ReactiveFormsModule,
    RouterModule.forChild([
      {
        path: '',
        component: TrackRecordComponent
      }
    ])
  ],
  exports: [
    RouterModule
  ]
})
export class TrackRecordModule { }
