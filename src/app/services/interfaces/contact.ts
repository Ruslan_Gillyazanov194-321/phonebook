export interface Contact {
  id: number,
  fullName: string,
  phoneNumber: string,
  created: Date,
  comment: string,
  favorite: boolean
}
