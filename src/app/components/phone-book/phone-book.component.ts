import { Component, OnInit } from '@angular/core';
import { ApiContactsService } from "../../services/api-contacts.service";
import { Contact } from "../../services/interfaces/contact";
import { FormControl, FormGroup, Validators } from "@angular/forms";
import { Title } from "@angular/platform-browser";
import { Router } from "@angular/router";

@Component({
  selector: 'app-phone-book',
  templateUrl: './phone-book.component.html',
  styleUrls: ['./phone-book.component.scss']
})
export class PhoneBookComponent implements OnInit {

  // Список контактов
  public contacts: Contact[] = []

  // Флаг отображения формы
  public addFormVisible: boolean = false

  // Форма добавления нового контакта
  public form: FormGroup;

  constructor(
    public apiContactService: ApiContactsService,
    private titleService: Title,
    private router: Router
  ) {
    this.titleService.setTitle('Phone book')
  }

  ngOnInit(): void {
    this.form = new FormGroup({
      fullName: new FormControl('', [
        Validators.required
      ]),
      phoneNumber: new FormControl('', [
        Validators.required
      ]),
      comment: new FormControl('', [
        Validators.required
      ]),
      favorite: new FormControl(true, [
        Validators.required
      ])
    });

    this.getContacts()
  }

  /**
   * Возвращает массив контактов и подписывается на них
   */
  getContacts() {
    this.apiContactService.users().subscribe(contacts => {
      this.contacts = contacts;
    })
  }

  /**
   * Фильтрует массив контактов по полю и значению
   */
  filterByFavorite(field: string, value: boolean) {
    this.apiContactService.filterContact(field, value).subscribe(contacts => {
      this.contacts = contacts
    })
  }

  /**
   * Удаляет контакт по id и фильтрует список текущих контактов.
   * @param id
   */
  removeContact(id: number) {
    this.apiContactService.remove(id).subscribe(() => {
      this.contacts = this.contacts.filter(contact => contact.id !== id)
    })
  }

  /**
   * Добавляет новый контакт.
   */
  submit() {
    if (this.form.valid) {
      const date = new Date()

      const contact = {
        ...this.form.value,
        created: `${date.getDate()}-${date.getMonth() + 1}-${date.getFullYear()}`
      };

      this.apiContactService.add(contact).subscribe(() => {
        this.getContacts()
      })

      // Сбрасываем форму
      this.form.reset()
      // Скрываем форму
      this.addFormVisible = false
    }
  }

  /**
   * Отображает форму редактирования контакта по указанному ID.
   * @param id
   */
  showEditContactForm(id: number) {
    this.router.navigate(['edit', id])
  }
}
