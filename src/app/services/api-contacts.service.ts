import { Injectable } from '@angular/core';
import { HttpClient, HttpParams } from "@angular/common/http";
import { Observable, throwError } from "rxjs";
import { Contact } from "./interfaces/contact";
import { catchError } from "rxjs/operators";

@Injectable({
  providedIn: 'root'
})
export class ApiContactsService {

  /**
   * Локальный json-server сервер
   * https://github.com/typicode/json-server
   * @private
   */
  private jsonServer: string = 'http://localhost:3000/users'

  constructor(private http: HttpClient) { }

  /**
   * Возвращает массив всех контактов.
   */
  users(): Observable<Contact[]> {
    return this.http.get<Contact[]>(this.jsonServer)
      .pipe(
        catchError(error => {
          console.log("Error: ", error.message)
          return throwError(error)
        })
      );
  }

  /**
   * Возвращает контакт по id.
   * @param id
   */
  getById(id: number): Observable<Contact> {
    return this.http.get<Contact>(this.jsonServer + '/' + id)
      .pipe(
        catchError(error => {
          console.log("Error: ", error.message)
          return throwError(error)
        })
      );
  }

  /**
   * Добавляет новый контакт.
   * @param contact
   */
  add(contact: Contact): Observable<Contact> {
    return this.http.post<Contact>(this.jsonServer, contact)
      .pipe(
        catchError(error => {
          console.log("Error: ", error.message)
          return throwError(error)
        })
      );
  }

  /**
   * Удаляет указаный контакт по id.
   * @param id
   */
  remove(id: number): Observable<void> {
    return this.http.delete<void>(this.jsonServer + '/' + id)
  }

  /**
   * Изменяет контакт по id.
   * @param id
   * @param body
   */
  change(id: number, body: Contact): Observable<Contact> {
    return this.http.put<Contact>(this.jsonServer + '/' + id, body)
      .pipe(
        catchError(error => {
          console.log("Error: ", error.message)
          return throwError(error)
        })
      );
  }

  /**
   * Фильтрует контакты по указанному полю
   * @param field
   * @param value
   */
  filterContact(field: string, value: any): Observable<Contact[]> {
    return this.http.get<Contact[]>(this.jsonServer, {
      params: new HttpParams().append(field, value)
    }).pipe(
      catchError(error => {
        console.log("Error: ", error.message)
        return throwError(error)
      })
    );
  }
}
